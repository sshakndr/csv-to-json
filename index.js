const fs = require('fs')
const encoding = require('encoding')

class Measurement{// Класс измерения
    constructor(operator = 'Неизвестен',date = null){// Конструктор, в котором все поля в том же порядке, как и в оригинальном файле
        this.operator = operator
        this.date = new Date(date)
        this.experiments = []
    }

    addExperiment(name,date,mg_L_concentration) {// Метод добавления эксперимента из таблицы в измерение(итоговый объект/файл)
        let new_experiment = new Experiment(name,date,mg_L_concentration)
        this.experiments.push(new_experiment)
    }
}

class Experiment{// Класс эксперимента(записи в таблице оригинального файла). Порядок таклй же
    constructor(name,date,mg_L_concentration){
        this.name = name.split(' ')[1]?name.split(' ')[1]:name
        this.date = new Date(date)
        this.mg_L_concentration = parseFloat(mg_L_concentration)
    }
}

function createJSON(filename){// Функция конвертации файла
    if (!fs.existsSync(filename)){// Проверка существования файла
        console.log('Файл не найден')
        return
    }
    let file = fs.readFileSync(filename)
    file = encoding.convert(file, 'UTF-8', 'WINDOWS-1251')// Смена кодировки буфера на UTF-8
    let lines = file.toString().split('\n')
    let index = 0
    let info = []
    while(index<lines.length){
        let line = lines[index].split(';')
        if (line[0]==='Оператор') info.push(line[1])
        if (line[0]==='Дата/Время') info.push(line[1])
        index++
        if (line[0]==='Наименование') break
    }
    let measurement = new Measurement(...info)
    while(index<lines.length-1){
        let line = lines[index].split(';')
        measurement.addExperiment(line[0],line[1],line[2])
        index++
    }
    let newname = filename.split('.')
    newname.pop()// Убрано расширение файла
    fs.writeFileSync(newname.join('.')+'.json',JSON.stringify(measurement,null,2))
}


// Вызов функции. Принимает путь фо файла или просто название, если он лежит в корневой папке
createJSON('файл для тестового задания.csv')